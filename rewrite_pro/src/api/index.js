import requset from "@/request";
// 一级分类的请求

export const reqCategoryList1 = () => {
  return requset.get("/admin/product/getCategory1");
};

export const reqCategoryList2 = (category1Id) => {
  return requset.get(`/admin/product/getCategory2/${category1Id}`);
};
