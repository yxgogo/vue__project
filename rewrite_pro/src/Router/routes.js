const routes = [
  {
    path: "/home",
    component: () => import("@/views/Home"),
    name: "Home",
  },
  {
    path: "/login",
    component: () => import("@/views/Login"),
    name: "Login",
    meta: {
      isHiddenFooter: true,
    },
  },
  {
    path: "/search",
    component: () => import("@/views/Search"),
    name: "Search",
  },
  {
    path: "/register",
    component: () => import("@/views/Register"),
    name: "Register",
    meta: {
      isHiddenFooter: true,
    },
  },

  {
    path: "",
    redirect: "/home",
  },
  {
    path: "/*",
    component: () => import("@/views/404"),
    name: "NotFound",
  },
];
export default routes;
