import { reqCategoryList1, reqCategoryList2 } from "@/api";
import Vue from "vue";

const state = {
  Category1List: [],
};

const mutations = {
  setCategory1List(state, payload) {
    state.Category1List = payload.Category1List;
  },

  addCategory1ListChildren(state, payload) {
    state.Category1List.forEach((item) => {
      if (item !== payload.item) return;
      Vue.set(item, "children", payload.Category1List);
    });
  },
};

const actions = {
  async getCategory1List(store) {
    let result = await reqCategoryList1();
    console.log(result);
    store.commit("setCategory1List", { Category1List: result });
  },

  async getCategory2List(store, category1Id) {
    let nowcategory = store.state.Category1List.find(
      (item) => item.id === category1Id
    );
    if (nowcategory.children) return;

    let result = await reqCategoryList2(category1Id);
    console.log(result);
    store.commit("addCategory1ListChildren", {
      item: nowcategory,
      Category1List: result,
    });
  },
};

const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
