import axios from "axios";

const request = axios.create({
  baseURL: process.env.VUE_APP_API,
  timeout: 5000,
  headers: {},
});

request.interceptors.request.use(
  (config) => {
    // Do something before request is sent
    return config;
  },
  (error) => {
    // Do something with request error
    return Promise.reject(error);
  }
);

request.interceptors.response.use(
  (response) => {
    // Do something before response is sent
    return response.data.data;
  },
  (error) => {
    // Do something with response error
    return Promise.reject(error);
  }
);

export default request;
