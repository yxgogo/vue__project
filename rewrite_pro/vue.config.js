const { defineConfig } = require("@vue/cli-service");
module.exports = defineConfig({
  transpileDependencies: true,
  configureWebpack: {
    resolve: {
      alias: {
        "@comp": "@/components",
      },
    },
  },
  devServer: {
    open: false,
    host: "localhost",
    port: 8086,

    proxy: {
      "dev-api1": {
        target: "http://gmall-h5-api.atguigu.cn",
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          "^/dev-api1": "",
        },
      },
    },
  },
});
