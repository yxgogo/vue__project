import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);
export default new Vuex.Store({
  state: { count: 0, num: 5 },
  mutations: {
    incrementN(state, payload) {
      state.count += payload.n;
    },
  },
  getters: {},
  actions: {
    incrementWait(store, b) {
      console.log(this);
      setTimeout(() => {
        store.commit("incrementN", { n: b });
      }, 2000);
    },
  },
});
