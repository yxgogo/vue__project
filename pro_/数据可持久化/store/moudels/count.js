const state = {
  count: localStorage.getItem("Count") || 0,
};
const mutations = {
  increment(state) {
    state.count++;
    localStorage.setItem("Count", state.count);
  },
};
const actions = {};
const getters = {};
export default {
  state,
  mutations,
  actions,
  getters,
};
