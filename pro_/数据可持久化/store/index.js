import Vue from "vue";
import VueX from "vuex";
import count from "@/store/moudels/count";
import move from "@/store/moudels/move";
Vue.use(VueX);
export default new VueX.Store({
  modules: {
    count,
    move,
  },
});
