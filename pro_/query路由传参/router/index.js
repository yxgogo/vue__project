import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);
export default new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/login",
      component: () => import("../views/Login"),
    },
    {
      path: "/home",
      component: () => import("../views/Home"),
      children: [
        {
          path: "music",
          component: () => import("../views/Home/components/Music.vue"),
          children: [
            {
              path: "opt",
              component: () => import("../views/Home/components/Item.vue"),
              name: "MusicItem",
            },
          ],
        },
        {
          path: "ys",
          component: () => import("../views/Home/components/Keephealth.vue"),
        },
        {
          path: "pase",
          component: () => import("../views/Home/components/Pasetime.vue"),
        },
      ],
    },
    {
      path: "",
      redirect: "/login",
    },
  ],
});
