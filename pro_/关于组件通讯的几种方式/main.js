// import Vue from "vue/dist/vue";
import Vue from "vue";
import App from "./App.vue";

Vue.config.productionTip = false;
// 全局注册过滤器
// Vue.filter("isNull", (value) => {
//   return value || "暂无数据";
// });
const myRole = "fudao";

//全局注册自定义指令
Vue.directive("role", (el, binding) => {
  console.log(binding.value);
  //假设某个按钮上的权限不包含当前我们自己的角色,需要把当前的按钮隐藏掉
  if (!binding.value.includes(myRole)) {
    el.style.display = "none";
    console.log(el);
    // el.remove();
    // el.parentNode.removeChild();
    // 在它未插入父节点  所以  无法 调用
    // 建议将  第二个参数  写成对象的形式  分别对应不同的周期
    // 类似于生命周期
  }
});

// 给所有的组件注册一个属性   因为 组件是通过组件函数实例化来的   它的原型链会访问到  Vue的原型对象
Vue.prototype.$C = "你好啊";
new Vue({
  render: (h) => h(App),
  // components: {
  //   App,
  // },
}).$mount("#app");
