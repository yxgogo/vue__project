import Vue from "vue";
import VueRouter from "vue-router";

import Home from "../views/Home";

import Secorn from "../views/Home/components/Secorn.vue";
import Show from "../views/Home/components/Show.vue";
import Move from "../views/Move";
Vue.use(VueRouter);
const router = new VueRouter({
  routes: [
    {
      path: "/home",
      component: Home,
      redirect: "/home/show",
      children: [
        {
          path: "show",
          component: Show,
        },
        {
          path: "learn",
          component: Secorn,
        },
      ],
    },
    {
      path: "/move/:id?",
      props: true,
      component: Move,
    },
    {
      path: "",
      redirect: "/home",
    },
  ],
});
export default router;
