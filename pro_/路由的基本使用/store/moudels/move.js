import axios from "axios";
const state = {
  movelist: JSON.parse(localStorage.getItem("Movelist")) || [],
};
const mutations = {
  setMovelist(state, payload) {
    state.movelist = payload.movelist;
    localStorage.setItem("Movelist", JSON.stringify(state.movelist));
  },
};
const actions = {
  async getMovelist(store) {
    const result = await axios.get(
      "https://pcw-api.iqiyi.com/search/recommend/list?channel_id=1&data_type=1&mode=11&page_id=2&ret_num=48&session=b9fd987164f6aa47fad266f57dffaa6a"
    );
    console.log(result);
    store.commit("setMovelist", { movelist: result.data.data.list });
  },
};
const getters = {};
export default {
  state,
  mutations,
  actions,
  getters,
};
