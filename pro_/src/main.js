import Vue from "vue";
import App from "./App.vue";

Vue.config.productionTip = false;

//使用Vue.use()使用插件
/* 
  use做了什么?
    1. 如果发现插件是一个对象,则默认调用对象内部的install方法,并传入Vue作为参数
    2. 如果发现插件是一个函数,则把这个函数单过是install方法,传入Vue作为参数
*/

new Vue({
  render: (h) => h(App),
}).$mount("#app");
