const state = {
  count: 0,
};
const mutations = {
  increment(state) {
    state.count++;
  },
};
const actions = {};
const getters = {};
export default {
  state,
  mutations,
  actions,
  getters,
};
