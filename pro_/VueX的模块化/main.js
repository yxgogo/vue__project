// import Vue from "vue/dist/vue";

import Vue from "vue";
import App from "./App.vue";
import store from "./store";
// 在入口文件  就引入   Vuex 创建的数据管理
Vue.config.productionTip = false;
Vue.prototype.$C = "你好啊";
new Vue({
  render: (h) => h(App),
  store: store,
  beforeCreate() {
    Vue.prototype.$bus = this;
  },
}).$mount("#app");
