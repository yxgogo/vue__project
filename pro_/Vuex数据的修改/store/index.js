import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);
export default new Vuex.Store({
  state: { count: 0, num: 5 },
  mutations: {
    increment(state, payload) {
      // 第一个参数  是他的数据  第二个是带来的参数 写成对象形式
      console.log(state, payload);
      state.count++;
    },
    direment(state) {
      state.count--;
    },
    incrementN(state, payload) {
      state.count += payload.n;
    },
    incrementWait(state, payload) {
      setTimeout(() => {
        state.count += payload.n;
      }, 2000);
    },
  },
  getters: {},
  actions: {},
});
