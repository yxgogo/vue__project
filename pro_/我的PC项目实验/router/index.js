import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);
const router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/login",
      component: () => import("../views/Login"),
      name: "Login",
    },
    {
      path: "/home",
      component: () => import("../views/Home"),
      name: "Home",
      children: [
        { path: "music", component: () => import("../views/Home/Main/Music") },
        { path: "radio", component: () => import("../views/Home/Main/Radio") },
      ],
      beforeEnter(to, from, next) {
        if (localStorage.getItem("token")) {
          next();
        } else {
          alert("请登录你的账号");
          next({ path: "/login" });
        }
      },
    },
    {
      path: "",
      redirect: "/login",
    },
  ],
});
router.beforeEach((to, from, next) => {
  console.log(to, from);
  next();
});
router.beforeResolve((to, from, next) => {
  console.log(to, from, next);
  next();
});

export default router;
