// import Vue from "vue/dist/vue";
import "./rest.css"; //样式重置
import Vue from "vue";
import App from "./App.vue";
import router from "./router";

import ElementUI from "element-ui";
Vue.use(ElementUI);
import "element-ui/lib/theme-chalk/index.css";
// 在入口文件  就引入   Vuex 创建的数据管理
Vue.config.productionTip = false;
Vue.prototype.$C = "你好啊";
new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
