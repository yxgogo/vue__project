import axios from "axios";
import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);
export default new Vuex.Store({
  state: { count: 0, movelist: [] },
  mutations: {
    incrementN(state, payload) {
      state.count += payload.n;
    },
    setMovelist(state, payload) {
      state.movelist = payload.movelist;
    },
  },
  getters: {
    moveName3_6(state) {
      let result = state.movelist.filter(
        (item) => item.name.length > 3 && item.name.length < 6
      );
      return result.length;
    },
  },
  actions: {
    async getMovelist(store) {
      const result = await axios.get(
        "https://pcw-api.iqiyi.com/search/recommend/list?channel_id=1&data_type=1&mode=11&page_id=2&ret_num=48&session=b9fd987164f6aa47fad266f57dffaa6a"
      );
      console.log(result);
      store.commit("setMovelist", { movelist: result.data.data.list });
    },
  },
});
