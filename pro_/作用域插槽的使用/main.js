// import Vue from "vue/dist/vue";
import Vue from "vue";
import App from "./App.vue";
Vue.config.productionTip = false;
Vue.prototype.$C = "你好啊";
new Vue({
  render: (h) => h(App),
  beforeCreate() {
    Vue.prototype.$bus = this;
  },
}).$mount("#app");
