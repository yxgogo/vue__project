import Vue from "vue";
import VueRouter from "vue-router";

import Home from "../views/Home";

import Secorn from "../views/Home/components/Secorn.vue";
import Show from "../views/Home/components/Show.vue";

Vue.use(VueRouter);
// router.push(location, onComplete?, onAbort?)
// 原生上的push方法   可以传3个参数，第一个为 跳转的地址，第二个时成功的回调，第三个是失败的回调
const oldPush = VueRouter.prototype.push;
VueRouter.prototype.push = function (
  location,
  onComplete = () => {},
  onAbort = () => {}
) {
  oldPush.call(this, location, onComplete, onAbort);
};
const router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/home",
      component: Home,
      redirect: "/home/show",
      children: [
        {
          path: "show",
          component: Show,
          name: "Show",
        },
        {
          path: "learn",
          component: Secorn,
          name: "Learn",
        },
      ],
    },

    {
      path: "",
      redirect: "/home",
    },
  ],
});
export default router;
