// import Vue from "vue/dist/vue";
import Vue from "vue";
import App from "./App.vue";

Vue.config.productionTip = false;
Vue.filter("isNull", (value) => {
  return value || "暂无数据";
});
console.log(Boolean(undefined));
new Vue({
  render: (h) => h(App),
  // components: {
  //   App,
  // },
}).$mount("#app");
