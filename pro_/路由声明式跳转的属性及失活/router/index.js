import Vue from "vue";
import VueRouter from "vue-router";

import Home from "../views/Home";
import Loging from "../views/Loging";
import Secorn from "../views/Home/components/Secorn.vue";
import Show from "../views/Home/components/Show.vue";

Vue.use(VueRouter);
const router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/home",
      component: Home,

      children: [
        {
          path: "show",
          component: Show,
          name: "Show",
        },
        {
          path: "learn",
          component: Secorn,
          name: "Learn",
        },
      ],
    },
    {
      path: "/login",
      component: Loging,
    },

    {
      path: "",
      redirect: "/login",
    },
  ],
});
export default router;
