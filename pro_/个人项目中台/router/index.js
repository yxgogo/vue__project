import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);
const router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/login",
      component: () => import("../views/Login"),
      name: "Login",
    },
    {
      path: "/home",
      redirect: "/home/mr",
      component: () => import("../views/Home"),
      name: "Home",
      children: [
        {
          path: "mr",
          component: () => import("../views/Home/components/Main"),
          name: "Home_page",
        },
        {
          path: "music",
          component: () => import("../pages/components/Music"),
          name: "Music",
        },
        {
          path: "radio",
          component: () => import("../pages/components/Radio"),
        },
      ],
    },
    {
      path: "",
      redirect: "/login",
    },
  ],
});
router.beforeEach((to, from, next) => {
  console.log(to, from);
  next();
});
router.beforeResolve((to, from, next) => {
  console.log(to, from, next);
  next();
});

export default router;
