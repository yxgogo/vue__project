import Vue from "vue";
import Vuex from "vuex";

import Category from "@/store/moudles/Category";
Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    Category,
  },
});
