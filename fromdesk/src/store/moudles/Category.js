import {
  reqCategory2List,
  reqCategoryList1,
  reqCategory3List,
} from "@/api/index";
import Vue from "vue";

const state = {
  //  初始化数据
  CategoryList1: [],
};
const mutations = {
  setCategotyList1(state, payload) {
    state.CategoryList1 = payload.CategoryList1;
  },

  addCategorylist1Children(state, payload) {
    state.CategoryList1.forEach((item) => {
      if (payload.item === item) {
        Vue.set(item, "children", payload.childrenlist);
      }
    });
  },
};
const getters = {};
const actions = {
  // 一级分类请求
  async getCategoryList1(store) {
    const result = await reqCategoryList1();
    // console.log(result);
    store.commit("setCategotyList1", { CategoryList1: result });
  },
  // 二级分类

  async getCategoryList2(store, category1Id) {
    let nowCategory = store.state.CategoryList1.find(
      (item) => item.id === category1Id
    );
    if (nowCategory.children) return;

    const result = await reqCategory2List(category1Id);

    // 在  请求二级的同时  请求  三级  把三级列表的数据添加在    二级数据里的children  里
    let reqCategory3ListPromiseArr = [];
    result.forEach((item) => {
      reqCategory3ListPromiseArr.push(reqCategory3List(item.id));
    });
    const Category3List = await Promise.all(reqCategory3ListPromiseArr);
    Category3List.forEach((item, index) => {
      result[index].children = item;
    });
    store.commit("addCategorylist1Children", {
      item: nowCategory,
      childrenlist: result,
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};
