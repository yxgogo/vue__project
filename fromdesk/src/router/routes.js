const routes = [
  {
    path: "/home",
    component: () => import("@/views/Home"),
    name: "Home",
  },
  {
    path: "/login",
    component: () => import("@/views/Login"),
    name: "Login",
    meta: {
      FooterIsHidden: true,
    },
  },
  {
    path: "/search/:keyword?",
    component: () => import("@/views/Search"),
    name: "Search",
    props(route) {
      return {
        ...route.query,
        ...route.params,
      };
    },
  },
  {
    path: "/register",
    component: () => import("@/views/Register"),
    name: "Register",
    meta: {
      FooterIsHidden: true,
    },
  },
  {
    path: "/detail/:skuId",
    component: () => import("@/views/Detail"),
    name: "Detail",
  },
  {
    path: "/addCartSuccess",
    component: () => import("@/views/AddCartSuccess"),
    name: "AddCartSuccess",
  },
  {
    path: "/shopcart",
    component: () => import("@/views/ShopCart"),
    name: "ShopCart",
  },
  {
    path: "/",
    redirect: "/home",
  },
  {
    path: "/*",
    component: () => import("@/views/404"),
    name: "NotFound",
  },
];

export default routes;
