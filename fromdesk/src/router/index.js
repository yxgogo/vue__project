// 引入     进度条
import nProgress from "nprogress";

import "nprogress/nprogress.css";

import Vue from "vue";

import VueRouter from "vue-router";

Vue.use(VueRouter);

import routes from "./routes";
// console.log(routes);
const router = new VueRouter({
  mode: "history",
  routes,
});

router.beforeEach((to, from, next) => {
  nProgress.start();
  next();
});
router.afterEach((to, from) => {
  nProgress.done();
});
export default router;
