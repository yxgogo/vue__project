import request from "@/serve/request";
import Mockrequest from "@/serve/Mockrequest";
export const reqCategoryList1 = () => {
  //    接口地址:/admin/product/getCategory1
  return request.get("/admin/product/getCategory1");
};

//2. 请求2级分类
export const reqCategory2List = (category1Id) => {
  return request.get(`/admin/product/getCategory2/${category1Id}`);
};

//3. 请求3级分类
export const reqCategory3List = (category2Id) => {
  return request.get(`/admin/product/getCategory3/${category2Id}`);
};

// 4. mock banner 数据

export const reqBannerList = () => {
  return Mockrequest.get(`/mock-api/banner`);
};

// 5.mock floor 数据

export const reqFloor = () => {
  return Mockrequest.get(`/mock-api/floor`);
};

// 6. 请求搜索页数据

export const reqSearchList = (searchParams) => {
  return request.post(`/api/list`, searchParams);
};

// 7.请求详情页数据
export const reqDeatilList = (skuId) => {
  return request.get(`/api/item/${skuId}`);
};

// 8.添加购物车页面  添加购物车请求

export const reqAddCartSuccess = (skuId, skuNum) => {
  return request.post(`/api/cart/addToCart/${skuId}/${skuNum}`);
};

//9.我的购物车页面
export const reqShopCart = () => {
  return request.get(`/api/cart/cartList`);
};
