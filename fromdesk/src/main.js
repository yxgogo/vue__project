import Vue from "vue";
import App from "./App.vue";
import store from "@/store";
import router from "@/router";
// console.log(process.env);
import TypeNav from "@/components/TypeNav";

import Pagination from "@/components/Pagination";
import "@/mock";
Vue.component("TypeNav", TypeNav);
Vue.component("Pagination", Pagination);
Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
  beforeCreate() {
    Vue.prototype.$bus = this;
  },
  router,
  store,
}).$mount("#app");
