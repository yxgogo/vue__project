import axios from "axios";

const Mockrequest = axios.create({
  baseURL: "",
  timeout: "",
  headers: {},
});

Mockrequest.interceptors.request.use((config) => {
  /* 
      进度条配置：
        - 如果想要在每次请求都要进度条，则在拦截器中配置
        - 如果想要路由切换的时候才有进度条,则在路由守卫中配置
    */
  return config;
});

//给当前的axios实例配置响应拦截器
Mockrequest.interceptors.response.use(
  (response) => {
    /* 只要能进入当前响应拦截器，就说明请求是成功的，但是不一定是我们想要的信息，我们还要拿到返回的数据再次判断是否是我们想要的信息 */
    if (response.data.code !== 200 && response.data.code !== 20000) {
      //此时虽然请求成功，但是不是我们想要的，我们可以把错误直接展示
      alert(response.data.message);
      //因为error对象固定有一个message属性，所以我们在抛出自己的错误的时候,我们可以把错误封装到一个对象的message属性上,方便后续处理
      return Promise.reject({ message: response.data.message });
    } else {
      return response.data.data;
    }
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default Mockrequest;
