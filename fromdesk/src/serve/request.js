import axios from "axios";

const request = axios.create({
  // baseURL: process.env.NODE_ENV === "development" ? "/dev-api1" : "/prod-api1",
  baseURL: process.env.VUE_APP_API,
  timeout: 5000,
  headers: {},
});

request.interceptors.request.use(
  (config) => {
    // Do something before request is sent
    return config;
  },
  (error) => {
    // Do something with request error
    return Promise.reject(error);
  }
);
request.interceptors.response.use(
  (response) => {
    if (response.data.code !== 200) {
      //    做一个错误 提示   如果得到的数据不对  响应的数据不对 可以拿到  不然
      // 直接返回数据，有些拿不到
      alert(response.data.message);
      return Promise.reject({ message: response.data.data.message });
    }
    return response.data.data;
  },
  (error) => {
    return Promise.reject(error);
  }
);
export default request;
