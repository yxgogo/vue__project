import floor from "@/mock/data/floor";
import Mock from "mockjs";
import banner from "./data/banner.json";

Mock.mock("/mock-api/banner", "get", () => {
  return {
    code: 200,
    message: "成功",
    data: banner,
    ok: true,
  };
});

Mock.mock("/mock-api/floor", "get", () => {
  return {
    code: 200,
    message: "成功",
    data: floor,
    ok: true,
  };
});
