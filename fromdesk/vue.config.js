const { defineConfig } = require("@vue/cli-service");
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    open: false,
    host: "localhost",
    port: 8000,
    proxy: {
      "dev-api1": {
        //目标服务器地址
        target: "http://gmall-h5-api.atguigu.cn",
        //是否在跨域的时候,把请求的源改为目标地址(一般写为true),这样目标地址就会放行
        changeOrigin: true,
        //如果需要去请求代理,则把请求地址的的前缀替换(前缀的作用只是为了找到对应的代理)
        pathRewrite: {
          "^/dev-api1": "",
        },
      },
      "dev-api2": {
        target: "http://dsadasda",
        changeOrigin: true,
        pathRewrite: {
          "^/dev-api2": "",
        },
        //是否开启WebSocket协议
        // ws: true,
        ws: true,
      },
    },
  },
  configureWebpack: {
    // 配置  路径的简写
    // 它自己已经默认   写入了  "@"  代表着  path.reslove(__dirname,'src')
    resolve: {
      alias: {
        "@comp": "@/components",
      },
    },
  },
});
